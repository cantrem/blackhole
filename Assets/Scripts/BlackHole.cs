﻿/**
 * Copyright © 2019 Cristian Baciu
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, 
 * You can obtain one at https://mozilla.org/MPL/2.0/.
 */

using System.Collections.Generic;
using UnityEngine;

public class BlackHole : MonoBehaviour
{
    [SerializeField] private Cubemap Starbox;
    [SerializeField] private Cubemap StarboxDebug;
    [SerializeField] private Cubemap Debugbox;

    [SerializeField] private GameObject EventHorizon;
    [SerializeField] private GameObject ISCO;
    [SerializeField] private GameObject PhotonSphere;
    [SerializeField] private GameObject PhotonSphereO;

    [SerializeField] private GameObject DistortionPlane;
    [SerializeField] private GameObject Skybox;

    public void SetBackground(int bg)
    {
        Cubemap texture = null;
        switch(bg)
        {
            case 0: // space
                texture = Starbox;
                break;
            case 1: // space + axis
                texture = StarboxDebug;
                break;
            case 2: // axis
                texture = Debugbox;
                break;
        }

        var targets = new List<GameObject> { DistortionPlane, Skybox };
        foreach (var go in targets)
        {
            var renderer = go.GetComponent<MeshRenderer>();
            var mat = renderer.sharedMaterial;
            mat.SetTexture("_EnvTex", texture);
        }
    }

    public void SetDistortionOpacity(float opacity)
    {
        MaterialPropertyBlock props = new MaterialPropertyBlock();
        props.SetFloat("_Opacity", opacity);

        DistortionPlane.GetComponent<MeshRenderer>().SetPropertyBlock(props);

        if(opacity == 0.0f)
        {
            DistortionPlane.SetActive(false);
            Skybox.SetActive(true);
        }
        else if(opacity == 1.0f)
        {
            DistortionPlane.SetActive(true);
            Skybox.SetActive(false);
        }
        else
        {
            DistortionPlane.SetActive(true);
            Skybox.SetActive(true);
        }
    }

    public void SetEH(bool visible)
    {
        EventHorizon.SetActive(visible);
    }

    public void SetISCO(bool visible)
    {
        ISCO.SetActive(visible);
    }

    public void SetPhotonSphereOverlay(bool visible)
    {
        PhotonSphereO.SetActive(visible);
    }

    void Start()
    {
        
    }

    void Update()
    {
    }
}
