﻿/**
 * Copyright © 2019 Cristian Baciu
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, 
 * You can obtain one at https://mozilla.org/MPL/2.0/.
 */

using UnityEngine;
using UnityEngine.UI;

public class BlackHoleController : MonoBehaviour
{
    [SerializeField] private Dropdown PredefSettings;
    [SerializeField] private Dropdown Background;
    [SerializeField] private Slider DistortionOpacity;
    [SerializeField] private Toggle EventHorizon;
    [SerializeField] private Toggle PhotonSphere;
    [SerializeField] private Toggle ISCO;

    [SerializeField] private BlackHole BH;

    void OnEnable()
    {
        int customIdx = 4;
        PredefSettings.onValueChanged.RemoveAllListeners();
        PredefSettings.onValueChanged.AddListener((int value) => {
            PredefSettings.interactable = false;
            switch(value)
            {
                case 0: // eye candy
                    Background.value = 0;
                    DistortionOpacity.value = 1.0f;
                    EventHorizon.isOn = false;
                    PhotonSphere.isOn = false;
                    ISCO.isOn = false;
                    break;
                case 1: // debug
                    Background.value = 1;
                    DistortionOpacity.value = 1.0f;
                    EventHorizon.isOn = false;
                    PhotonSphere.isOn = false;
                    ISCO.isOn = false;
                    break;
                case 2: // distortion overlay
                    Background.value = 0;
                    DistortionOpacity.value = 0.5f;
                    EventHorizon.isOn = false;
                    PhotonSphere.isOn = false;
                    ISCO.isOn = false;
                    break;
                case 3: // debug overlay
                    Background.value = 1;
                    DistortionOpacity.value = 0.5f;
                    EventHorizon.isOn = false;
                    PhotonSphere.isOn = false;
                    ISCO.isOn = false;
                    break;
            }
            PredefSettings.interactable = true;
        });

        Background.onValueChanged.RemoveAllListeners();
        Background.onValueChanged.AddListener((int value) => {
            BH.SetBackground(value);
            if(PredefSettings.interactable)
                PredefSettings.value = customIdx;
        });

        DistortionOpacity.onValueChanged.RemoveAllListeners();
        DistortionOpacity.onValueChanged.AddListener((float value) => {
            BH.SetDistortionOpacity(value);
            if (PredefSettings.interactable)
                PredefSettings.value = customIdx;
        });

        EventHorizon.onValueChanged.RemoveAllListeners();
        EventHorizon.onValueChanged.AddListener((bool isOn) => {
            BH.SetEH(isOn);
            if (PredefSettings.interactable)
                PredefSettings.value = PredefSettings.value = customIdx;
        });

        PhotonSphere.onValueChanged.RemoveAllListeners();
        PhotonSphere.onValueChanged.AddListener((bool isOn) => {
            BH.SetPhotonSphereOverlay(isOn);
            if (PredefSettings.interactable)
                PredefSettings.value = customIdx;
        });

        ISCO.onValueChanged.RemoveAllListeners();
        ISCO.onValueChanged.AddListener((bool isOn) => {
            BH.SetISCO(isOn);
            if (PredefSettings.interactable)
                PredefSettings.value = customIdx;
        });

        PredefSettings.value = 1;
        PredefSettings.value = 0;

        BH.SetBackground(0);
        BH.SetDistortionOpacity(1.0f);
        BH.SetEH(false);
        BH.SetPhotonSphereOverlay(false);
        BH.SetISCO(false);
    }
}
