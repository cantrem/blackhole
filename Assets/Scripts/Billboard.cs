﻿/**
 * Copyright © 2019 Cristian Baciu
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, 
 * You can obtain one at https://mozilla.org/MPL/2.0/.
 */

using UnityEngine;

public class Billboard : MonoBehaviour
{
    public enum Mode
    {
        Full,
        Horizontal
    }

    [SerializeField] public Transform Camera;
    [SerializeField] public Vector3 ThisForward;
    [SerializeField] public Vector3 ThisUp;
    [SerializeField] public Mode BillboardMode = Mode.Full;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        switch(BillboardMode)
        {
            case Mode.Full:
                {
                    // TODO: fix this
                    Vector3 lookTo = Camera.position - transform.position;
                    
                    Quaternion fwdRot = Quaternion.FromToRotation(ThisForward, lookTo);
                    Quaternion upRot = Quaternion.FromToRotation(fwdRot * ThisUp, Vector3.up);

                    transform.rotation = upRot * fwdRot;
                }
                break;
            case Mode.Horizontal:
                {
                    Vector3 lookTo = Camera.position - transform.position;
                    lookTo.y = 0.0f;
                    Quaternion fwdRot = Quaternion.FromToRotation(ThisForward, lookTo);
                    Quaternion upRot = Quaternion.FromToRotation(fwdRot * ThisUp, Vector3.up);

                    transform.rotation = upRot * fwdRot;
                }
                break;
            default:
                return;
        }
    }
}
