﻿/**
 * Copyright © 2019 Cristian Baciu
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, 
 * You can obtain one at https://mozilla.org/MPL/2.0/.
 */

using UnityEngine;

namespace UTools.Cams
{
    /// <summary>
    /// Camera that orbits around its gameobject parent.
    /// Vertical and horizontal axis can be set to either global or parent.
    /// </summary>
    public class OrbitCamMotor : MonoBehaviour
    {
        /// <summary>
        /// [0.0-1.0 prc/s]
        /// Inertial movement parameter.
        /// Greater values stop movement more rapidly.
        /// </summary>
        [SerializeField] public float RotationDampening = 2.0f;
        /// <summary>
        /// [deg/s]
        /// When inertial rotation falls below this speed, it is stopped.
        /// </summary>
        [SerializeField] public float RotationStopThreshold = 0.1f;
        /// <summary>
        /// [deg]
        /// Minimum vertical angle from initial position.
        /// To enable vertical clamping, min/max values have to be in the (-90,+90) interval.
        /// </summary>
        [SerializeField] public float MinVertAngle = -80.0f;
        /// <summary>
        /// [deg]
        /// Maximum vertical angle from initial position.
        /// To enable vertical clamping, min/max values have to be in the (-90,+90) interval.
        /// </summary>
        [SerializeField] public float MaxVertAngle = 80.0f;
        /// <summary>
        /// [deg]
        /// Minimum horizontal angle from initial position.
        /// To enable horizontal clamping, min/max values have to be in the (-180,+180) interval.
        /// </summary>
        [SerializeField] public float MinHorAngle = -180.0f;
        /// <summary>
        /// [deg]
        /// Maximum horizontal angle from initial position.
        /// To enable horizontal clamping, min/max values have to be in the (-180,+180) interval.
        /// </summary>
        [SerializeField] public float MaxHorAngle = 180.0f;
        /// <summary>
        /// Set to true to ignore parent's rotation, which makes the camera orbit in global space.
        /// </summary>
        [SerializeField] public bool IgnoreParentRotation = false;
        
        /// <summary>
        /// [0.0-1.0 prc/s]
        /// Inertial movement parameter.
        /// Greater values stop movement more rapidly.
        /// </summary>
        [SerializeField] public float ZoomDampening = 4.0f;
        /// <summary>
        /// [dist units/s] when used with ZoomMode.Liniar
        /// [0.0-1.0 prc/s] when used with ZoomMode.Logarithmic
        /// </summary>
        [SerializeField] public float ZoomStopThreshold = 0.01f;
        /// <summary>
        /// [dist units] Minimum distance from (0,0,0) when zooming. Must be strictly positive.
        /// </summary>
        [SerializeField] public float MinZoomDistance = 2.0f;
        /// <summary>
        /// [dist units] Maximum distance from (0,0,0) when zooming. Set to 0 for infinite.
        /// </summary>
        [SerializeField] public float MaxZoomDistance = 50.0f;

        /// <summary>
        /// Clears active inertial rotation, resets tracked rotation for min/max angles.
        /// </summary>
        public void Reset()
        {
            smoothRot = Vector2.zero;
            totalRotation = Vector2.zero;
            smoothZoom = 0.0f;
        }

        /// <param name="rotation">[degrees] horizontal and vertical rotation</param>
        public void Rotate(Vector2 rotation, MotorMode mode)
        {
            rotate(rotation);
            if(mode == MotorMode.Inertial)
            {
                smoothRot = rotation;
                smoothRotMMode = mode;
            }
        }

        /// <param name="zoom">
        /// [dist units] if zMode == Liniar
        /// [0.0-1.0 prc] if zMode == Logarithmic
        /// </param>
        public void Zoom(float zoom, ZoomMode zMode, MotorMode mMode)
        {
            this.zoom(zoom, zMode);
            if(mMode == MotorMode.Inertial)
            {
                smoothZoom = zoom;
                smoothZoomZMode = zMode;
                smoothZoomMMode = mMode;
            }
        }

        void Start()
        {
            Reset();
        }

        void Update()
        {
            // inertial rot
            if(smoothRot.magnitude < RotationStopThreshold * Mathf.Deg2Rad)
            {
                smoothRot = Vector2.zero;
            }
            else
            {
                smoothRot.x = Mathf.Lerp(smoothRot.x, 0.0f, RotationDampening * Time.deltaTime);
                smoothRot.y = Mathf.Lerp(smoothRot.y, 0.0f, RotationDampening * Time.deltaTime);
                rotate(smoothRot);
            }
            
            // inertial zoom
            if(Mathf.Abs(smoothZoom) < ZoomStopThreshold)
            {
                smoothZoom = 0.0f;
            }
            else
            {
                smoothZoom = Mathf.Lerp(smoothZoom, 0.0f, ZoomDampening * Time.deltaTime);
                zoom(smoothZoom, smoothZoomZMode);
            }
        }

        private Vector2 clampRot(Vector2 rotation)
        {
            if (MinHorAngle > -180.0f) rotation.x = Mathf.Max(MinHorAngle - totalRotation.x, rotation.x);
            if (MaxHorAngle < 180.0f) rotation.x = Mathf.Min(MaxHorAngle - totalRotation.x, rotation.x);

            if (MinVertAngle > -90.0f) rotation.y = Mathf.Max(MinVertAngle - totalRotation.y, rotation.y);
            if (MaxVertAngle < 90.0f) rotation.y = Mathf.Min(MaxVertAngle - totalRotation.y, rotation.y);

            return rotation;
        }

        private float clampZoom(float zoom, float currDistance)
        {
            if (MinZoomDistance < 0.0001f) // magic threshold so we don't lose direction info
                MinZoomDistance = 0.0001f;

            zoom = Mathf.Max(MinZoomDistance - currDistance, zoom);
            if(MaxZoomDistance > 0.0f)
                zoom = Mathf.Min(MaxZoomDistance - currDistance, zoom);

            return zoom;
        }

        private void zoom(float zoom, ZoomMode zMode)
        {
            float currDistance = transform.localPosition.magnitude;
            if (zMode == ZoomMode.Logarithmic)
            {
                zoom = currDistance * zoom;
            }
            zoom = clampZoom(zoom, currDistance);

            transform.localPosition = transform.localPosition.normalized * (currDistance + zoom);
        }

        private void rotate(Vector2 rotation)
        {
            rotation = clampRot(rotation);
            totalRotation += rotation;

            var parentLocRot = transform.parent != null ? transform.parent.localRotation : Quaternion.identity;
            var parentPos = transform.parent != null ? transform.parent.position : Vector3.zero;

            var up = Vector3.up;
            var right = transform.localRotation * Vector3.right;

            if(!IgnoreParentRotation)
            {
                up = parentLocRot * up;
                right = parentLocRot * right;
            }

            transform.RotateAround(parentPos, right, rotation.y);
            transform.RotateAround(parentPos, up, rotation.x);
        }

        Vector2 smoothRot;
        MotorMode smoothRotMMode;
        Vector2 totalRotation;

        float smoothZoom;
        MotorMode smoothZoomMMode;
        ZoomMode smoothZoomZMode;
    }
}