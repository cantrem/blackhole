﻿/**
 * Copyright © 2019 Cristian Baciu
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, 
 * You can obtain one at https://mozilla.org/MPL/2.0/.
 */

using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;
using UTools.UInput;

namespace UTools.Cams
{
    /// <summary>
    /// Default controller for an Orbit Camera.
    /// Translates user input (keyboard, mouse) and feeds it to an OrbitCamMotor.
    /// </summary>
    public class OrbitCamController : MonoBehaviour, IKeyPressedHandler, IMouseMovedHandler, IAxisHandler
    {
        /// <summary>
        /// Keyboard rotation input key.
        /// </summary>
        [SerializeField] public KeyCode RotUpKey = KeyCode.W;
        /// <summary>
        /// Keyboard rotation input key.
        /// </summary>
        [SerializeField] public KeyCode RotDownKey = KeyCode.S;
        /// <summary>
        /// Keyboard rotation input key.
        /// </summary>
        [SerializeField] public KeyCode RotRightKey = KeyCode.D;
        /// <summary>
        /// Keyboard rotation input key.
        /// </summary>
        [SerializeField] public KeyCode RotLeftKey = KeyCode.A;
        /// <summary>
        /// Which key should be pressed when mouse rotation is desired.
        /// Use Keycode.None to always rotate.
        /// </summary>
        [SerializeField] public KeyCode MouseRotKey = KeyCode.Mouse0;

        /// <summary>
        /// Keyboard rotation motor mode.
        /// </summary>
        [SerializeField] public MotorMode KeyRotMotorMode = MotorMode.Crisp;
        /// <summary>
        /// Mouse/touch rotation motor mode.
        /// </summary>
        [SerializeField] public MotorMode PointerRotMotorMode = MotorMode.Inertial;
        /// <summary>
        /// Enable or disable mouse/touch rotation.
        /// </summary>
        [SerializeField] public bool PointerRot;

        /// <summary>
        /// [deg/sec]
        /// Rotation speed for keyboard input.
        /// </summary>
        [SerializeField] public float NormalRotSpeed = 45.0f;
        /// <summary>
        /// [mult]
        /// Multiplier used to convert pixel-space distances to rotation
        /// </summary>
        [SerializeField] public float PointerRotSpeedMult = 0.1f;
        /// <summary>
        /// [deg/sec]
        /// Maximum rotation speed for pointer input. Set to 0 to disable.
        /// </summary>
        [SerializeField] public float MaxPointerRotSpeed = 180.0f;

        /// <summary>
        /// Keyboard zoom input key.
        /// </summary>
        [SerializeField] public KeyCode ZoomInKey = KeyCode.E;
        /// <summary>
        /// Keyboard zoom input key.
        /// </summary>
        [SerializeField] public KeyCode ZoomOutKey = KeyCode.Q;

        /// <summary>
        /// Keyboard zoom motor mode.
        /// </summary>
        [SerializeField] public MotorMode KeyZoomMotorMode = MotorMode.Crisp;
        /// <summary>
        /// Keyboard zoom  mode.
        /// </summary>
        [SerializeField] public ZoomMode KeyZoomMode = ZoomMode.Logarithmic;
        /// <summary>
        /// Mouse wheel motor mode.
        /// </summary>
        [SerializeField] public MotorMode WheelZoomMotorMode = MotorMode.Inertial;
        /// <summary>
        /// Mouse wheel zoom mode.
        /// </summary>
        [SerializeField] public ZoomMode WheelZoomMode = ZoomMode.Liniar;

        /// <summary>
        /// [0.0-1.0 prc/sec]
        /// Zoom speed for keyboard input.
        /// </summary>
        [SerializeField] public float NormalZoomSpeed = 2.0f;
        /// <summary>
        /// [0.0-1.0 prc/sec]
        /// Zoom speed used for mouse wheel input.
        /// </summary>
        [SerializeField] public float WheelZoomSpeed = 2.0f;

        [SerializeField] private OrbitCamMotor Motor = null;

        void OnEnable()
        {
            Assert.IsNotNull(Motor, "OrbitCamController: Motor not set.");

            InputEventProvider.Instance.Register(this);
        }

        void OnDisable()
        {
            InputEventProvider.Instance.Unregister(this);
        }

        // Start is called before the first frame update
        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {
            if (rotVertDir != 0.0f || rotHorDir != 0.0f)
            {
                Vector2 rot =  new Vector2(rotHorDir, rotVertDir);
                rot = rot.normalized * NormalRotSpeed * Time.deltaTime;
                Motor.Rotate(rot, KeyRotMotorMode);
            }

            if(zoomDir != 0.0f)
            {
                float zoom = zoomDir * NormalZoomSpeed * Time.deltaTime;
                Motor.Zoom(zoom, KeyZoomMode, KeyZoomMotorMode);
            }

            resetInput();
        }

        public void OnKeyPressed(KeyboardEventData eventData)
        {
            handleKeyRot(eventData);
            handleKeyZoom(eventData);
        }

        public void OnMove(MouseEventData eventData)
        {
            if (MouseRotKey != KeyCode.None && !Input.GetKey(MouseRotKey))
                return;

            Vector2 rot = new Vector2(eventData.delta.x, -eventData.delta.y) * PointerRotSpeedMult;
            if(MaxPointerRotSpeed > 0.0f)
            {
                float rotSpeed = Mathf.Min(rot.magnitude, MaxPointerRotSpeed * Time.deltaTime);
                rot = rot.normalized * rotSpeed;
            }

            Motor.Rotate(rot, PointerRotMotorMode);
        }

        public void OnAxisChanged(AxisChangeEventData eventData)
        {
            if (eventData.id != 0)
                return;

            float zoom = -eventData.delta * WheelZoomSpeed;
            Motor.Zoom(zoom, WheelZoomMode, WheelZoomMotorMode);
        }

        private void handleKeyRot(KeyboardEventData eventData)
        {
            int dir = eventData.pressed ? 1 : -1;

            if (eventData.key == RotUpKey)
            {
                rotVertDir += dir;
                return;
            }

            if (eventData.key == RotDownKey)
            {
                rotVertDir -= dir;
                return;
            }

            if (eventData.key == RotLeftKey)
            {
                rotHorDir += dir;
                return;
            }

            if (eventData.key == RotRightKey)
            {
                rotHorDir -= dir;
                return;
            }
        }

        private void handleKeyZoom(KeyboardEventData eventData)
        {
            int dir = eventData.pressed ? 1 : -1;

            if(eventData.key == ZoomInKey)
            {
                zoomDir += -1 * dir;
                return;
            }
            if(eventData.key == ZoomOutKey)
            {
                zoomDir += dir;
                return;
            }

        }

        private void resetInput()
        {
            rotHorDir = 0;
            rotVertDir = 0;
            zoomDir = 0;
        }

        private int rotHorDir;
        private int rotVertDir;
        private int zoomDir;
    }

}