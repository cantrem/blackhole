﻿/**
 * Copyright © 2019 Cristian Baciu
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, 
 * You can obtain one at https://mozilla.org/MPL/2.0/.
 */

namespace UTools.Cams
{
    /// <summary>
    /// Used with camera movements (rotation, panning, zoom).
    /// Crisp generates an constant movement.
    /// Inertial generates an instant movement followed by a progressive dampening until the
    /// movement stops.
    /// </summary>
    public enum MotorMode
    {
        Crisp,
        Inertial
    }

    /// <summary>
    /// Liniar zoom will preserve the speed with which a camera changes the distance to the zoom 
    /// target.
    /// Logarithmic zoom will preserve the time it takes for a camera to go twice as close or far
    /// from the zoom target at any given distance.
    /// </summary>
    public enum ZoomMode
    {
        Liniar,
        Logarithmic
    }
}