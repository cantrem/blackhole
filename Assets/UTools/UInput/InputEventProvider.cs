﻿/**
 * Copyright © 2019 Cristian Baciu
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, 
 * You can obtain one at https://mozilla.org/MPL/2.0/.
 */

//#define UINPUT_DEBUG

using System;
using System.Collections.Generic;
using UnityEngine;

namespace UTools.UInput
{
    public struct KeyState
    {
        public bool pressed;
        public int lastPressTimestamp; // TODO
    }

    public class KeyboardEventData
    {
        public KeyboardEventData(KeyCode key, bool pressed)
        {
            this.key = key;
            this.pressed = pressed;
        }

        public KeyCode key { get; private set; }
        public bool pressed { get; private set; }
    }

    public class MouseEventData
    {
        public MouseEventData(Vector2 position, Vector2 delta, bool leftClick, bool rightClick)
        {
            this.position = position;
            this.delta = delta;
            this.leftClick = leftClick;
            this.rightClick = rightClick;
        }

        public Vector2 position { get; private set; }
        public Vector2 delta { get; private set; }
        public bool leftClick { get; private set; }
        public bool rightClick { get; private set; }
    }

    public class AxisChangeEventData
    {
        public AxisChangeEventData (string name, int id, float delta)
        {
            this.name = name;
            this.id = id;
            this.delta = delta;
        }

        public string name { get; private set; }
        public int id { get; private set; }
        public float delta { get; private set; }
    }

    /// <summary>
    /// Dispatches keyboard and mouse events to subscribers.
    /// </summary>
    public class InputEventProvider
    {
        public static InputEventProvider Instance
        {
            get { if (instance == null) instance = new InputEventProvider(); return instance; }
        }

        public KeyCode[] KeyCodeList { get; private set; }
        public KeyState[] CurrentKeyState { get; private set; }
        public Vector2 CurrentMousePos { get; set; }
        public List<string> AxisNames { get; private set; }

        public void Register(IInputHandler target)
        {
            if (target is IKeyDownHandler)
            {
                downHandlers.Add((IKeyDownHandler)target);
            }
            if (target is IKeyUpHandler)
            {
                upHandlers.Add((IKeyUpHandler)target);
            }
            if (target is IKeyPressedHandler)
            {
                pressHandlers.Add((IKeyPressedHandler)target);
            }
            if(target is IMouseMovedHandler)
            {
                moveHandlers.Add((IMouseMovedHandler)target);
            }
            if(target is IAxisHandler)
            {
                axisHandlers.Add((IAxisHandler)target);
            }
        }

        public void Unregister(IInputHandler target)
        {
            if (target is IKeyDownHandler)
            {
                downHandlers.Remove((IKeyDownHandler)target);
            }
            if (target is IKeyUpHandler)
            {
                upHandlers.Remove((IKeyUpHandler)target);
            }
            if (target is IKeyPressedHandler)
            {
                pressHandlers.Remove((IKeyPressedHandler)target);
            }
            if (target is IMouseMovedHandler)
            {
                moveHandlers.Remove((IMouseMovedHandler)target);
            }
            if (target is IAxisHandler)
            {
                axisHandlers.Remove((IAxisHandler)target);
            }
        }

        public void Update()
        {
            updateKeyboard();
            updateMouse();
            updateAxis();
        }

        private void updateKeyboard()
        {
            for (int i = 0; i < CurrentKeyState.Length; ++i)
            {
                if (CurrentKeyState[i].pressed != lastKeyState[i].pressed)
                {
                    if (CurrentKeyState[i].pressed)
                    {
                        // key down event
                        var data = new KeyboardEventData(KeyCodeList[i], true);
#if (UINPUT_DEBUG)
                        Debug.Log("InputEventProvider: Keydown " + data.key.ToString());
#endif
                        foreach (var handler in downHandlers)
                        {
                            handler.OnKeyDown(data);
                        }
                    }
                    else
                    {
                        // key up event
                        var data = new KeyboardEventData(KeyCodeList[i], false);
#if (UINPUT_DEBUG)
                        Debug.Log("InputEventProvider: Keyup " + data.key.ToString());
#endif

                        foreach (var handler in upHandlers)
                        {
                            handler.OnKeyUp(data);
                        }
                    }
                }
                if (CurrentKeyState[i].pressed)
                {
                    // key press event
                    var data = new KeyboardEventData(KeyCodeList[i], true);
#if (UINPUT_DEBUG)
                        Debug.Log("InputEventProvider: Keypressed " + data.key.ToString());
#endif

                    foreach (var handler in pressHandlers)
                    {
                        handler.OnKeyPressed(data);
                    }
                }
            }

            {
                var tmp = lastKeyState;
                lastKeyState = CurrentKeyState;
                CurrentKeyState = tmp;
            }
        }

        private void updateMouse()
        {
            if (CurrentMousePos == lastMousePos)
            {
                return;
            }

            if(lastMousePos.x < 0.0f) // unknown previous mouse position, don't send events
            {
                lastMousePos = CurrentMousePos;
                return;
            }

            Vector2 delta = CurrentMousePos - lastMousePos;
            var data = new MouseEventData(CurrentMousePos, delta, Input.GetKey(KeyCode.Mouse0), Input.GetKey(KeyCode.Mouse1));

            foreach (var handler in moveHandlers)
            {
                handler.OnMove(data);
            }

            lastMousePos = CurrentMousePos;
        }

        private void updateAxis()
        {
            for(int i = 0; i < AxisNames.Count; ++i)
            {
                string axisName = AxisNames[i];

                float d = Input.GetAxis(axisName);
                if (d == 0.0f)
                    continue;

                var data = new AxisChangeEventData(axisName, i, d);

                foreach (var handler in axisHandlers)
                {
                    handler.OnAxisChanged(data);
                }
            }
        }

        private InputEventProvider()
        {
            KeyCodeList = (KeyCode[])Enum.GetValues(typeof(KeyCode));
            CurrentKeyState = new KeyState[KeyCodeList.Length];
            lastKeyState = new KeyState[KeyCodeList.Length];
            AxisNames = new List<string>();
        }

        private HashSet<IKeyDownHandler> downHandlers = new HashSet<IKeyDownHandler>();
        private HashSet<IKeyPressedHandler> pressHandlers = new HashSet<IKeyPressedHandler>();
        private HashSet<IKeyUpHandler> upHandlers = new HashSet<IKeyUpHandler>();
        private HashSet<IMouseMovedHandler> moveHandlers = new HashSet<IMouseMovedHandler>();
        private HashSet<IAxisHandler> axisHandlers = new HashSet<IAxisHandler>();

        private KeyState[] lastKeyState;
        private Vector2 lastMousePos = new Vector2(-1.0f, -1.0f);

        private static InputEventProvider instance = null;
    }

    public interface IInputHandler
    { }

    /// <summary>
    /// Event triggered on the frame a key is pressed.
    /// </summary>
    public interface IKeyDownHandler : IInputHandler
    {
        void OnKeyDown(KeyboardEventData eventData);
    }

    /// <summary>
    /// Event triggered each frame a key is pressed.
    /// </summary>
    public interface IKeyPressedHandler : IInputHandler
    {
        void OnKeyPressed(KeyboardEventData eventData);
    }

    /// <summary>
    /// Event triggered on the frame a key is released.
    /// </summary>
    public interface IKeyUpHandler : IInputHandler
    {
        void OnKeyUp(KeyboardEventData eventData);
    }

    /// <summary>
    /// Event triggered on the frame the mouse moved.
    /// </summary>
    public interface IMouseMovedHandler : IInputHandler
    {
        void OnMove(MouseEventData eventData);
    }

    /// <summary>
    /// Event triggered when an axis input is not 0.
    /// </summary>
    public interface IAxisHandler : IInputHandler
    {
        void OnAxisChanged(AxisChangeEventData eventData);
    }
}
