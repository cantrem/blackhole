﻿/**
 * Copyright © 2019 Cristian Baciu
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, 
 * You can obtain one at https://mozilla.org/MPL/2.0/.
 */

//#define UINPUT_DEBUG

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Assertions;

namespace UTools.UInput
{
    /// <summary>
    /// Should be put on a screen-wide dummy GUI element that should get focus when world
    /// input is desired. Will enable and disable any input providers subscribed to InputProviders.
    /// </summary>
    public class WorldInputDispatcher : Selectable, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField] public List<MonoBehaviour> InputProviders = new List<MonoBehaviour>();
        [SerializeField] public EventSystem EventSystem = null;
        [SerializeField] public bool DefaultEnabled = false;
        [SerializeField] public bool MouseHoverFocuses = true;

        protected override void Start()
        {
            if (EventSystem == null)
            {
                EventSystem = FindObjectOfType<EventSystem>();
                Assert.IsNotNull(EventSystem, "WorldInputDispatcher: cannot find EventSystem.");
            }

            SetActive(DefaultEnabled);
        }

        public void SetActive(bool value)
        {
#if UINPUT_DEBUG
            Debug.Log("World Input: " + (value ? "active" : "inactive"));
#endif
            foreach (MonoBehaviour component in InputProviders)
            {
                if (component == null)
                    continue;

                component.enabled = value;
            }
        }

        override public void OnSelect(BaseEventData data)
        {
            SetActive(true);
        }

        override public void OnDeselect(BaseEventData data)
        {
            SetActive(false);
        }

        override public void OnPointerEnter(PointerEventData e)
        {
            if (!MouseHoverFocuses)
                return;
            if (e.dragging)
                return;

            EventSystem.SetSelectedGameObject(this.gameObject);
        }

        override public void OnPointerExit(PointerEventData e)
        {
            if (!MouseHoverFocuses)
                return;

            EventSystem.SetSelectedGameObject(null);
        }
    }
}