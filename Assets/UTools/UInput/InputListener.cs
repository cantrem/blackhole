﻿/**
 * Copyright © 2019 Cristian Baciu
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, 
 * You can obtain one at https://mozilla.org/MPL/2.0/.
 */

using System.Collections.Generic;
using UnityEngine;

namespace UTools.UInput
{
    /// <summary>
    /// Make sure that in your Project Settings InputListener has a higher execution priority than the event handler scripts.
    /// </summary>
    public class InputListener : MonoBehaviour
    {
        [SerializeField] private List<string> Axis = new List<string>(new []{ "Mouse ScrollWheel" });

        void OnEnable()
        {
            if (instance != null && instance != this)
            {
                Debug.LogWarning("InputListener: trying to make a 2nd instance, disabling it.");
                instance.enabled = false;
            }
            instance = this;
        }

        void Update()
        {
            var eventProvider = InputEventProvider.Instance;

            KeyCode[] keys = eventProvider.KeyCodeList;
            KeyState[] state = eventProvider.CurrentKeyState;

            for (int i = 0; i < keys.Length; ++i)
            {
                state[i].pressed = Input.GetKey(keys[i]);
            }

            eventProvider.CurrentMousePos = Input.mousePosition;

            eventProvider.AxisNames.Clear();
            eventProvider.AxisNames.AddRange(Axis);

            eventProvider.Update();
        }

        private static InputListener instance = null;
    }
}
