﻿Shader "Unlit/BG_Skybox"
{
    Properties
    {
        _EnvTex ("Texture", Cube) = "defaulttexture" {}
    }
    SubShader
    {
        Tags  { "Queue" = "Background" "RenderType" = "Opaque" }
        LOD 100

        Pass
        {
            ZWrite Off
            ZTest Always
            Cull Front

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float4 vertexWorld : TEXCOORD0;
            };

            uniform samplerCUBE _EnvTex;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.vertexWorld = mul(unity_ObjectToWorld, v.vertex) - float4(_WorldSpaceCameraPos, 0.0);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = texCUBE(_EnvTex, i.vertexWorld);
                return col;
            }
            ENDCG
        }
    }
}
