﻿Shader "BH/BH_Distortion"
{
    Properties
    {
        _EnvTex("Env", Cube) = "defaulttexture" {}
        _Opacity("Opacity", Float) = 1.0
    }
    SubShader
    {
        Tags { "Queue" = "Background+1" "RenderType" = "Transparent" }
        Blend SrcAlpha OneMinusSrcAlpha
        LOD 100

        Pass
        {
            ZWrite Off
            ZTest Always

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 v : POSITION;
            };

            struct v2f
            {
                float4 v : SV_POSITION;
                float3 env : TEXCOORD0; // worldspace ray from eye to point on plane
                float3 center : TEXCOORD1; // worldspace ray from eye to plane center (really not needed as a varying)
                float2 locV : TEXCOORD2;
            };

            uniform samplerCUBE _EnvTex;
            uniform float _Opacity;

            // https://gist.github.com/patricknelson/f4dcaedda9eea5f5cf2c359f68aa35fd
            float4 setAxisAngle(float3 axis, float rad) {
                rad = rad * 0.5;
                float s = sin(rad);
                return float4(s * axis[0], s * axis[1], s * axis[2], cos(rad));
            }

            float4 multQuat(float4 q1, float4 q2) {
                return float4(
                    q1.w * q2.x + q1.x * q2.w + q1.z * q2.y - q1.y * q2.z,
                    q1.w * q2.y + q1.y * q2.w + q1.x * q2.z - q1.z * q2.x,
                    q1.w * q2.z + q1.z * q2.w + q1.y * q2.x - q1.x * q2.y,
                    q1.w * q2.w - q1.x * q2.x - q1.y * q2.y - q1.z * q2.z
                    );
            }

            float3 rotateVector(float4 quat, float3 vec) {
                // https://twistedpairdevelopment.wordpress.com/2013/02/11/rotating-a-vector-by-a-quaternion-in-glsl/
                float4 qv = multQuat(quat, float4(vec, 0.0));
                return multQuat(qv, float4(-quat.x, -quat.y, -quat.z, quat.w)).xyz;
            }

            v2f vert (appdata i)
            {
                v2f o;
                o.v = UnityObjectToClipPos(i.v);

                float4 worldPos = mul(unity_ObjectToWorld, i.v);
                float4 worldCenter = mul(unity_ObjectToWorld, float4(0.0, 0.0, 0.0, 1.0));

                o.env = worldPos - _WorldSpaceCameraPos;
                o.center = worldCenter - _WorldSpaceCameraPos;
                
                float2 worldScale = float2(
                    length(float3(unity_ObjectToWorld[0].x, unity_ObjectToWorld[1].x, unity_ObjectToWorld[2].x)),
                    length(float3(unity_ObjectToWorld[0].y, unity_ObjectToWorld[1].y, unity_ObjectToWorld[2].y))
                    );

                o.locV = float2(i.v.x * worldScale.x, i.v.y * worldScale.y);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                //fixed4 envCol = texCUBE(_EnvTex, i.env);

                float r = length(i.locV);
                float deflAngle = 2.0 * 1.0 / (r - 1.5); // assuming Schwarzschild radius is 1

                float3 rotAxis = cross(i.center, i.env);
                rotAxis = normalize(rotAxis);
                float4 q = setAxisAngle(rotAxis, deflAngle);
                float3 envSampleDir = rotateVector(q, i.env);

                //fixed4 envCol = texCUBE(_EnvTex, envSampleDir);
                fixed4 envCol = texCUBE(_EnvTex, envSampleDir);
                //envSampleDir = normalize(envSampleDir);
                //fixed4 envRayCol = fixed4(envSampleDir.x, envSampleDir.y, envSampleDir.z, 1.0);

                fixed4 col = envCol;
                col.a = col.a * _Opacity;
                //col = fixed4(r, r, r, 1.0);
                return col;
            }
            ENDCG
        }
    }
}
