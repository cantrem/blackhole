﻿Shader "BH/BH_AccretionDisk"
{
    Properties
    {
        //_EnvTex ("Env", Cube) = "defaulttexture" {}
        _Opacity("Opacity", Float) = 1.0
    }
        SubShader
        {
            Tags { "Queue" = "Background+1" "RenderType" = "Transparent" }
            Blend SrcAlpha OneMinusSrcAlpha
            LOD 100

            Pass
            {
                CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag

                #include "UnityCG.cginc"

                struct appdata
                {
                    float4 v : POSITION;
                };

                struct v2f
                {
                    float4 v : SV_POSITION;
                };

                //uniform samplerCUBE _EnvTex;
                uniform float _Opacity;

                // https://gist.github.com/patricknelson/f4dcaedda9eea5f5cf2c359f68aa35fd
                float4 setAxisAngle(float3 axis, float rad) {
                    rad = rad * 0.5;
                    float s = sin(rad);
                    return float4(s * axis[0], s * axis[1], s * axis[2], cos(rad));
                }

                float4 multQuat(float4 q1, float4 q2) {
                    return float4(
                        q1.w * q2.x + q1.x * q2.w + q1.z * q2.y - q1.y * q2.z,
                        q1.w * q2.y + q1.y * q2.w + q1.x * q2.z - q1.z * q2.x,
                        q1.w * q2.z + q1.z * q2.w + q1.y * q2.x - q1.x * q2.y,
                        q1.w * q2.w - q1.x * q2.x - q1.y * q2.y - q1.z * q2.z
                        );
                }

                float3 rotateVector(float4 quat, float3 vec) {
                    // https://twistedpairdevelopment.wordpress.com/2013/02/11/rotating-a-vector-by-a-quaternion-in-glsl/
                    float4 qv = multQuat(quat, float4(vec, 0.0));
                    return multQuat(qv, float4(-quat.x, -quat.y, -quat.z, quat.w)).xyz;
                }

                v2f vert(appdata i)
                {                   
                    //float3 rotAxis = float3(1.0, 0.0, 0.0);
                    
                    float3 rotAxis = cross(normalize(i.v.xyz), float3(0.0, 0.0, -1.0));

                    float r = length(i.v);
                    float deflAngle = 2.0 * 1.0 / (r - 1.5); // assuming Schwarzschild radius is 1
                    deflAngle = clamp(deflAngle * i.v.y / -10.0, 0.0, 1.0);

                    float4 q = setAxisAngle(rotAxis, deflAngle);
                    i.v = float4(rotateVector(q, i.v), 1.0);

                    v2f o;
                    o.v = UnityObjectToClipPos(i.v);
                    return o;
                }

                fixed4 frag(v2f i) : SV_Target
                {
                    fixed4 col = fixed4(1.0, 1.0, 1.0, 1.0);
                    return col;
                }
                ENDCG
            }
        }
}
