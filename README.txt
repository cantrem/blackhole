Black hole visualizer made over the weekend using Unity3D.
Showcases shaders, camera and user input handling.

Start with Assets/Scenes/Scene.unity

Works on Unity 2019.2.21f1.

Copyright � 2019 Cristian Baciu